﻿using System;
using System.Diagnostics;

namespace Managed
{
    static class Program
    {
        static void Main()
        {
            var worker = new NativeWorker();
            worker.Progress += (s, e) =>
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(e.Value);
                Console.ResetColor();
            };

            worker.DoWork(31);

            Console.WriteLine("\nEnd.");
            if (Debugger.IsAttached) Console.ReadKey();
        }
    }
}
