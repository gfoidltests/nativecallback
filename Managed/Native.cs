﻿using System.Runtime.InteropServices;

namespace Managed
{
    internal static class Native
    {
        // Attribute is in that simple case not absolutely necessary.
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void ProgressCallback(int value);
        //---------------------------------------------------------------------
        // MarshalAs is not necessary, because it is clear what it is.
        [DllImport("Native")]
        public static extern void do_work(int count, [MarshalAs(UnmanagedType.FunctionPtr)] ProgressCallback progressCallback);
    }
}
