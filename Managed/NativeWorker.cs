﻿using System;

namespace Managed
{
    public class NativeWorker
    {
        public class ProgressEventArgs : EventArgs
        {
            public int Value { get; }
            //-----------------------------------------------------------------
            public ProgressEventArgs(int value) => this.Value = value;
        }
        //---------------------------------------------------------------------
        public event EventHandler<ProgressEventArgs> Progress;
        //---------------------------------------------------------------------
        public void DoWork(int count)
        {
            Native.do_work(count, this.OnProgress);
        }
        //---------------------------------------------------------------------
        private void OnProgress(int value) => this.Progress?.Invoke(this, new ProgressEventArgs(value));
    }
}
