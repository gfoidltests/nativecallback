#pragma once
//-----------------------------------------------------------------------------
#ifdef __cplusplus
    #ifdef NATIVE_EXPORTS
        #define DLL_API extern "C" __declspec(dllexport)
    #else
        #define DLL_API extern "C" __declspec(dllimport)
    #endif // Native_EXPORTS
#else
    #ifdef NATIVE_EXPORTS
        #define DLL_API __declspec(dllexport)
    #else
        #define DLL_API __declspec(dllimport)
    #endif // Native_EXPORTS
#endif
