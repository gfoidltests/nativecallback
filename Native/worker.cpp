#include "worker.h"
#include <iostream>
//-----------------------------------------------------------------------------
using namespace std;
//-----------------------------------------------------------------------------
void do_work(const int count, ProgressCallback progressCallback)
{
    try
    {
        for (int i = 0; i < count; ++i)
        {
            cout << "native: " << i << endl;

            if (i % 10 == 0)
            {
                if (progressCallback)
                    progressCallback(i);
            }
        }
    }
    catch (const exception& e)
    {
        cout << e.what() << endl;
    }
}
