#pragma once
//-----------------------------------------------------------------------------
#include "dll_api.h"
#include "callbacks.h"
//-----------------------------------------------------------------------------
DLL_API void do_work(const int count, ProgressCallback progressCallback);
